import React from 'react'
import { graphql } from 'react-relay'

import { useConfig } from '@saastack/core'
import { ErrorComponent, Loading } from '@saastack/components'
import { useQuery } from '@saastack/relay'

import DepartmentMaster from '../components/DepartmentMaster'

import { DepartmentPageQuery } from '../__generated__/DepartmentMaster_departments.graphql'

const query = graphql`
    query DepartmentPageQuery($parent: String) {
        ...DepartmentMaster_departments @arguments(parent: $parent)
    }
`

const DepartmentPage = () => {
    const { companyId } = useConfig()
    const { data, loading, error, refetch } = useQuery<DepartmentPageQuery>(query, {
        parent: companyId,
    })

    if (loading) {
        return <Loading />
    }
    if (error) {
        return <ErrorComponent error={error} />
    }

    return <DepartmentMaster parent={companyId} departments={data} />
}

export default DepartmentPage
