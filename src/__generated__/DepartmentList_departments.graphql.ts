/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentList_departments = ReadonlyArray<{
    readonly id: string;
    readonly name: string;
    readonly description: string;
    readonly " $refType": "DepartmentList_departments";
}>;
export type DepartmentList_departments$data = DepartmentList_departments;
export type DepartmentList_departments$key = ReadonlyArray<{
    readonly " $data"?: DepartmentList_departments$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentList_departments">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DepartmentList_departments",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Department",
  "abstractKey": null
};
(node as any).hash = '56b3d74918aa4a1f92e2750531ac7a28';
export default node;
