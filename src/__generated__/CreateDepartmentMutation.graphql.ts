/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type CreateDepartmentInput = {
    clientMutationId?: string | null;
    department?: DepartmentInput | null;
    parent?: string | null;
};
export type DepartmentInput = {
    description?: string | null;
    id?: string | null;
    metadata?: string | null;
    name?: string | null;
};
export type CreateDepartmentMutationVariables = {
    input?: CreateDepartmentInput | null;
};
export type CreateDepartmentMutationResponse = {
    readonly createDepartment: {
        readonly clientMutationId: string;
        readonly payload: {
            readonly id: string;
            readonly name: string;
            readonly description: string;
        };
    };
};
export type CreateDepartmentMutation = {
    readonly response: CreateDepartmentMutationResponse;
    readonly variables: CreateDepartmentMutationVariables;
};



/*
mutation CreateDepartmentMutation(
  $input: CreateDepartmentInput
) {
  createDepartment(input: $input) {
    clientMutationId
    payload {
      id
      name
      description
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateDepartmentPayload",
    "kind": "LinkedField",
    "name": "createDepartment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Department",
        "kind": "LinkedField",
        "name": "payload",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CreateDepartmentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CreateDepartmentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "8a4a05715b75f9e7f795f25b36ae4805",
    "id": null,
    "metadata": {},
    "name": "CreateDepartmentMutation",
    "operationKind": "mutation",
    "text": "mutation CreateDepartmentMutation(\n  $input: CreateDepartmentInput\n) {\n  createDepartment(input: $input) {\n    clientMutationId\n    payload {\n      id\n      name\n      description\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '804738dc58594d55c79fcbe2123bd4dd';
export default node;
