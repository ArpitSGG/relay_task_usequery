/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentPageQueryVariables = {
    parent?: string | null;
};
export type DepartmentPageQueryResponse = {
    readonly " $fragmentRefs": FragmentRefs<"DepartmentMaster_departments">;
};
export type DepartmentPageQuery = {
    readonly response: DepartmentPageQueryResponse;
    readonly variables: DepartmentPageQueryVariables;
};



/*
query DepartmentPageQuery(
  $parent: String
) {
  ...DepartmentMaster_departments_2XQG37
}

fragment DepartmentMaster_departments_2XQG37 on Query {
  departments(parent: $parent) {
    department {
      id
      name
      description
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "parent"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "parent",
    "variableName": "parent"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "DepartmentPageQuery",
    "selections": [
      {
        "args": (v1/*: any*/),
        "kind": "FragmentSpread",
        "name": "DepartmentMaster_departments"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "DepartmentPageQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "ListDepartmentResponse",
        "kind": "LinkedField",
        "name": "departments",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Department",
            "kind": "LinkedField",
            "name": "department",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "675aac5212a3e033c4d7aa92a57f76b0",
    "id": null,
    "metadata": {},
    "name": "DepartmentPageQuery",
    "operationKind": "query",
    "text": "query DepartmentPageQuery(\n  $parent: String\n) {\n  ...DepartmentMaster_departments_2XQG37\n}\n\nfragment DepartmentMaster_departments_2XQG37 on Query {\n  departments(parent: $parent) {\n    department {\n      id\n      name\n      description\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '686737c0decd884dc92f6a55f9a7a4f1';
export default node;
