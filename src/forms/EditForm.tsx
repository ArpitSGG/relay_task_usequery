import { Toggle } from '@saastack/components'
import { Form, Input, Textarea } from '@saastack/forms'
import { FormProps } from '@saastack/forms/types'
import React from 'react'

import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

interface Props extends FormProps<DepartmentInput> {
    type: 'NEW' | 'UPDATE'
}

const EditForm: React.FC<Props> = ({ type, ...props }) => {
    const [show, setShow] = React.useState(false)

    return (
        <Form {...props}>
            <Input large name="name" label="Title" grid={{ xs: 12 }} />
            <Toggle
                label={type === 'NEW' ? 'Add description' : 'Update description'}
                show={show}
                onShow={setShow}
            >
                <Textarea grid={{ xs: 12 }} label="Description" name="description" />
            </Toggle>
        </Form>
    )
}

export default EditForm
