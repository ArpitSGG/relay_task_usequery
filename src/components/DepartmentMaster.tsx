import React, { useState } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'

import { AddOutlined } from '@material-ui/icons'
import { Typography } from '@material-ui/core'

import { Layout } from '@saastack/layouts'

import DepartmentList from './DepartmentList'
import { useAlert } from '@saastack/core'
import { ActionItem } from '@saastack/components/Actions'
import { DepartmentPageQueryResponse } from '../__generated__/DepartmentMaster_departments.graphql'
import EditDepartment from './EditDepartment'
import DeleteDepartment from './DeleteDepartment'

import DeleteDepartmentMutation from '../mutations/DeleteDepartmentMutation'
import CreateDepartmentMutation from '../mutations/CreateDepartmentMutation'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'
import UpdateDepartmentMutation from '../mutations/UpdateDepartmentMutation'

type Props = {
    parent: string | undefined
    departments: DepartmentPageQueryResponse | null
}

const deafultDepartment = {
    id: '',
    name: '',
    description: '',
}

const DepartmentMaster = ({
    parent,
    departments: {
        departments: { department },
    },
}: Props) => {
    const showAlert = useAlert()
    const environment = useRelayEnvironment()
    const variables = { parent }

    const [openEdit, setOpenEdit] = useState<{
        type: 'NEW' | 'UPDATE'
        open: boolean
        department: any
    }>({
        type: 'NEW',
        open: false,
        department: deafultDepartment,
    })
    const [openDelete, setOpenDelete] = useState<{ open: boolean; department: null | string }>({
        open: false,
        department: null,
    })

    const [isLoading, setIsLoading] = useState(false)

    const closeEditDepartment = () => {
        setOpenEdit({
            type: 'NEW',
            open: false,
            department: null,
        })
    }
    const openEditDepartment = (type: 'NEW' | 'UPDATE', department: any = null) => {
        setOpenEdit({
            type,
            open: true,
            department,
        })
    }

    const openDeleteDepartment = (department: string) => {
        setOpenDelete({ open: true, department })
    }
    const closeDeleteDepartment = () => {
        setOpenDelete({ open: false, department: null })
    }

    const handleDeleteDepartment = () => {
        if (!openDelete.department) return
        setIsLoading(true)
        DeleteDepartmentMutation.commit(environment, variables, openDelete.department, {
            onSuccess: () => {
                // refetch()
                showAlert(<>Department deleted successfully</>, {
                    variant: 'info',
                })
                setIsLoading(false)
                closeDeleteDepartment()
            },
            onError: (e: string) => {
                setIsLoading(false)
                closeDeleteDepartment()
                showAlert(e, {
                    variant: 'error',
                })
            },
        })
    }

    const handleEditDepartment = (type: 'NEW' | 'UPDATE', department: DepartmentInput) => {
        if (type === 'NEW') {
            CreateDepartmentMutation.commit(environment, variables, department, {
                onSuccess: () => {
                    setIsLoading(false)
                    showAlert(<>Department added successfully!</>, {
                        variant: 'info',
                    })
                    closeEditDepartment()
                },
                onError: (e: string) => {
                    setIsLoading(false)
                    showAlert(e, {
                        variant: 'error',
                    })
                    closeEditDepartment()
                },
            })
        } else {
            UpdateDepartmentMutation.commit(environment, department, ['name', 'description'], {
                onSuccess: () => {
                    setIsLoading(false)
                    showAlert(<>Department updated successfully!</>, {
                        variant: 'info',
                    })
                    closeEditDepartment()
                },
                onError: (e: string) => {
                    setIsLoading(false)
                    showAlert(e, {
                        variant: 'error',
                    })
                    closeEditDepartment()
                },
            })
        }
    }
    const actions: ActionItem[] = [
        {
            icon: AddOutlined,
            title: <Typography>Add</Typography>,
            onClick: () => openEditDepartment('NEW'),
        },
    ]

    return (
        <Layout
            header="Departments"
            subHeader="Tushhharr"
            col1={
                <DepartmentList
                    departments={department}
                    onClick={openEditDepartment}
                    onDelete={openDeleteDepartment}
                />
            }
            actions={actions}
        >
            {openEdit.open && (
                <EditDepartment
                    onClose={closeEditDepartment}
                    type={openEdit.type}
                    onSubmit={handleEditDepartment}
                    initialValues={
                        openEdit.type === 'NEW' ? deafultDepartment : openEdit.department
                    }
                    loading={isLoading}
                />
            )}
            {openDelete.open && (
                <DeleteDepartment
                    onClose={closeDeleteDepartment}
                    handleDelete={handleDeleteDepartment}
                    loading={isLoading}
                />
            )}
        </Layout>
    )
}

export default createFragmentContainer(DepartmentMaster, {
    departments: graphql`
        fragment DepartmentMaster_departments on Query
        @argumentDefinitions(parent: { type: "String" }) {
            departments(parent: $parent) {
                department {
                    id
                    name
                    description
                }
            }
        }
    `,
})
